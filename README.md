# VRCrash SDK Offline

## ¿Cómo instalar?

Se debe hacer clone en una de las carpetas de assets de dentro de un proyecto cualquiera de Unity versión 2018.4.20f1. Puedes [descargarlo desde Unity Hub](unityhub://2018.4.20f1/008688490035). Con eso ya estás preparado para funcionar.

## Features

- Validación de habitaciones con requisitos bien definidos.
- Generador de unitypackage con nombre específico.
- Explicación de como se hace la habitación.

## Requisitos de la habitación

- Máximo de triángulos: 50.000
- Máximo de materiales: 10
- Máximo de luces: Ninguno, pero se puede usar emisión en los materiales.
- Los materiales solo pueden ser 'Standard', si quieres usar otro shader contactar con los admins.
- Los componontes que se pueden usar son: 
    - TransformComponent.
    - MeshFilterComponent.
    - MeshRendererComponent.
    - UdonBehaviour (contactar con los admins).
- No se pueden promocionar ningún contenido.
- No se pueden poner portales a otros mundos.
- Si queréis tener espejos solo debéis preparar el espejo y los botones, nosotros añadimos la funcionalidad. Si queréis probarlos solo tenéis que contactar con alguno de los admins
 y os ayudarán con el prefab de Udon para probarlo.
> Podeis hacer lo que queráis con vuestra escena de Unity pero solo los materiales de: pared interior, suelo, techo, placa y lo incluido dentro del GameObject [Pon tus modelos aqui dentro] será lo que se tenga en cuenta y 
genere el unitypackage que nos pasaréis. 
> Es recomendado usar el [SDK3 de mundos de VRChat]().

## Tutorial para subir habitación

1. Instalar Unity (recomendada versión 2018.4.20f1), puedes [descargarlo desde Unity Hub](unityhub://2018.4.20f1/008688490035)(recomendado) o directamente desde [este enlace](https://unity3d.com/es/get-unity/download?thank-you=update&download_nid=63664&os=Win).
2. Crear en Unity un proyecto 3D.
3. Debes importar el SDK en tu proyecto de Unity arrastrándolo.
4. Aparecerá una nueva pestaña en el menú superior que ponga VRCrash.
5. Dentro del menú seleccionar la opción Panel de Control.
6. Introducir tu nombre de usuario de VRChat y pulsar el botón generar plantilla habitación. Se eliminará todo lo de la escena por completo CUIDADO CON ESTE PASO.
7. Ahora puedes editar todo lo de la habitación metiendo los objetos que quieras en la sección [Pon tus modelos aqui dentro].
8. Según introduzcas nuevos "GameObjects" el SDK irá validando si cumplen las restricciones. Aparecerán indicadas en el SDK.
9. Una vez termineis de editar la habitación y cumpla los requisitos aparecerá una opción para generar la habitación final. Esto generará
    un unitypackage (está en la carpeta UnityPackages dentro de Resources).
10. Puedes generar cuantas habitaciones quieras, mientras mantengas el mismo proyecto se generará un nuevo unitypackge con un nuevo número de versión.
11. Una vez estéis contentos con la habitación nos paseis el último UnityPackage que habéis generado.

Informar de que además de esto, nosotros validaremos que la habitación cumpla unos mínimos. Por ejemplo, si alguien coloca un cuadro y vemos que está volando
preguntaremos si era la intención sino ayudaremos a corregir este tipo de detalles. De todas formas no dudéis en preguntar en cualquier momento, solemos
estar por discord e intentaremos ayudar con lo que sea.

## Dudas:
1. **¿Si hay otros objetos en la escena se deben de eliminar?**

No es necesario eliminarlos, el paquete solo se genera del GameObject de la habitación. Sin embargo,
al generar el template se elimina todo lo de la escena.

2. **¿Cuales son los requisitos?**

Mirar la sección de Requisitos.

3. **¿Qué hago si quiero ir cambiando mi habitación antes de la subida de la mansión?**

Mientras se mantengan los nombres de la habitación por defecto, cada versión de tu habitación
pondrá automáticamente el número de versión actual.

4. **¿Hay que guardar las texturas en algún sitio en específico?**

Al presionar el botón de 'Generar paquete de la habitación' ya se incluyen las dependencias. De todas formas,
es importantísimo guardar y organizar las texturas por si hubiese algún error.

5. **¿Qué hago si tengo otro shader que no es el 'Standard'?**

Contanta con nosotros y valoraremos incluirlo.

6. **¿Qué pasa si tengo un objecto fuera de la habitación?**

Lo borraremos automáticamente.

7. **¿La habitación que se genere con el botón es la que estará en la mansión?**

No necesariamente, la habitación pasa 2 validaciones. Una automática del SDK y una manual
en la cual se valorará con otros criterios si la habitación es correcta.

8. **¿Por qué no se puede poner más de una luz?**

Porque penaliza el rendimiento y aún no sabemos como haremos el tema de la iluminación.

9. **¿Por qué no puedo promocionar mi contenido en mi habitación?**

Varios motivos. Uno es que el mapa tiene el objetivo de ser la casa de VRCrash y en nuestra casa
no ponemos publicidad. Segundo, porque no vamos a validar ninguna fuente de lo que habeis creado,
si lo que promocionais es de otra persona, si el contenido es explícito, etc etc...
nosotros no queremos tener nada que ver con ello. Para promocionar algo haremos una sección en la
mansión, se pondrá un anuncio para los que quieran promocionar lo que sea puedan contactar con
nosotros.

10. **¿Se puede sobrepasar alguna de las restricciones?**

En la mayoría de casos no, pero existe cierta flexibilidad. En caso de que quieras sobrepasar
alguna de ellas contacta con nosotros y analizaremos si es posible.

11. **¿Por qué hay dos prefabs de la habitación?**

Porque existen dos tipos de habitaciones según la zona de la mansión en la que estén. No se puede
cambiar el tipo salvo casos excepcionales.

12. **¿Qué pasa si tengo habitación compartida?**

Poneros de acuerdo ya que cualquiera de los dos puede hacer la habitación pero solo el dueño puede
subirla.

13. **¿Qué texturas puedo editar?**

Hay una carpeta llamada Resources -> Texturas y ahí dentro aparecerán todas las texturas por defecto.
La textura de la puerta no será la definitiva y aunque la edites no estará en tu habitación
ya que todas las habitaciones tendrán las mismas puertas. El resto son totalmente editables. Puedes añadir nuevas
texturas pero las paredes exterior y el suelo de la entrada no se tendrán en cuenta.

También se han incluído los PSDs de las texturas para facilitar la edición. Además se han incluído las imágenes
de los UVs por si os sirven de algo.

*Aclaración: La puerta, la ventana, la pared de fuera de la habitación y el suelo de fuera de la habitación no son editables.*

14. **¿Qué resolución de las texturas debo usar?**

1024x1024. Si usáis una resolución mayor tener en cuenta que la bajaremos a esa.

15. **Las paredes interior tienen unos salientes arriba y abajo, ¿eso se verá?**

No, eso es para que sea más sencillo de visualizar la habitación, luego el modelo se optimizará lo máximo posible eliminando cualquier
cara que no se vea, esas incluidas.

16. **¿Cómo organizar el proyecto?**

Mi recomendación es crear tu propia carpeta y llamarla: Habitación. Ahí dentro crear una carpeta para:
- Materiales
- Texturas
- Modelos

Y todos lo que uséis para vuestra habitación (aunque sean los materiales o texturas por defecto) lo mováis/creéis ahí y empecéis solo editéis lo de vuestra carpeta.

17. **¿Hay tiempo límite?**

Desde el momento en el que se publique este SDK por primera vez, el mismo día del mes siguiente. Puedes verlo en la sección de anuncios del discord.

18. **¿Qué hago si tengo un error que no entiendo?**

Contacta con los admins en discord.

19. **¿Qué hago si encuentro un error en el SDK?**

Contacta con los admins en discord para que podamos crear una tarea para solucionarlo.

## Distribución de las habitaciones:

### Habitación nº 1
- Dueño: giovimix
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 2
- Dueño: theoriginalzero
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 3
- Dueño: gonth
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 4
- Dueño: kopynh
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 5
- Dueño: bshadows
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 6
- Dueño: vulob1
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.
### Habitación nº 7
- Dueño: m.cris
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.
### Habitación nº 8
- Dueño: hikarifandubs
- Compañero: akarinee
- Tipo de habitación: Tipo 2.
### Habitación nº 9
- Dueño: mrhunterhothot
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.
### Habitación nº 10
- Dueño: ????
- Compañero: ????
- Tipo de habitación: Tipo 2.
### Habitación nº 11
- Dueño: alfonso01
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 12
- Dueño: saidairu
- Compañero: puppymoonx
- Tipo de habitación: Tipo 1.
### Habitación nº 13
- Dueño: wizuky
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 14
- Dueño: rose shirayama
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 15
- Dueño: sebdestro
- Compañero: Ninguno.
- Tipo de habitación: Tipo 1.
### Habitación nº 16
- Dueño: live_dimensions
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.
### Habitación nº 17
- Dueño: nao05
- Compañero: srhostile
- Tipo de habitación: Tipo 2.
### Habitación nº 18
- Dueño: hecatia7
- Compañero: hanjamon
- Tipo de habitación: Tipo 2.
### Habitación nº 19
- Dueño: tokiru
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.
### Habitación nº 20
- Dueño: moon_ray
- Compañero: Ninguno.
- Tipo de habitación: Tipo 2.