# Changelog VRCrash SDK Offline

## [1.2.0]

### Añadido

- [29](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/29) Soporte al Shader de Xiexes.
- [30](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/30) Soporte al Shader de Poyomi
- [28](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/28) Añadir música genkey
- [25](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/25) Validar play mode mensaje para no hacer nada


### Modificado

- [32](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/32) Actualización base de datos de miembros
- [31](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/31) Solucionar problemas de logging

## [1.1.0]

### Añadido

- [3](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/3) La seccion about us. 


## [1.0.1]

### Añadido
 
- [27](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/27) Fuente Aware-bold (la de VRCrash).
- [21](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/21) Validación de directorios (Prefab y UnityPackages.) más botones para creación de los mismos.

## Solucionado

- [21](https://gitlab.com/team-crash/vrcrash-sdk-offline/-/issues/21) Se valida si existen los directorios de Prefab y UnityPackages.


## [1.0.0]

### Añadido

- Validación de número de luces permitido.
- Validación de número de materiales permitido.
- Validación de la máxima cantidad de triángulos permitida.
- PSDs de las paredes de los dos tipos de habitación, del techo, suelo y placa.
- Soporte para el shader 'standard' de unity.
- Botón para generar paquete si se cumplen las restricciones.
- Base de datos offline con los usuarios y su habitación de la mansión.
- Banner del SDK.
- Prefabs de las habitaciones.
- Menú de tutorial.
- Menú de panel de control.
- Generación de template de la mansión.
- Pantalla de 'Log In'.
- Clases con todos los literales de la aplicación.
- Cerrar las ventanas mantiene la sesión.
- Un PopUp para confirmar la limpieza de toda la escena.
- Añadido 'scrolling' en la información de validación.
- Botón para cambiar de usuario.
- Botón para salir de la ventana.
- Información de versión del SDK.
- Información de sesión de usuario.
- Control de versiones de los paquetes de habitaciones generadas.
- Lector de JSON.
- Menu de información.
- Easter egg.

### Modificado

 - Habitación y textures
 
 ### Solucionado

- Los mesh filter ya no pierden la referencia en el prefab.
- Error de repintado.
- Los directorios de Prefabs y UnityPackages ahora también están incluidos.
- Ahora la habitación se valida según el tipo de usuario.
- Ya no aparece la barra horizontal en el paso de Creación.
- Texturas y PSDs no bien organizados.
- El botón de generar paquete ahora se oculta si tu habitación no es la correcta.