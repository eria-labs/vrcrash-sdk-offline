﻿using System;

[Serializable]
public class VRCR_Users
{
    public VRCR_User[] users;

    public VRCR_User[] Users
    {
        get => users;
        set => users = value;
    }
}