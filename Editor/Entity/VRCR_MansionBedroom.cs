﻿using System;
using VRCrash.Editor.Naming;

[Serializable]
public class VRCR_MansionBedroom
{
    public string owner;
    public string partner;
    public short number;

    public string Owner
    {
        get => owner;
        set => owner = value;
    }

    public string Partner
    {
        get => partner;
        set => partner = value;
    }

    public short Number
    {
        get => number;
        set => number = value;
    }

    public string GetBedroomTypeTag()
    {
        if (number < 6 || (number > 10 && number < 16)) return VRCR_MessagesNaming.TagBedroomType1;
        return VRCR_MessagesNaming.TagBedroomType2;
    }
}