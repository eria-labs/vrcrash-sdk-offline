﻿using System;

[Serializable]
public class VRCR_User
{
    public string name;
    public string displayName;
    public VRCR_MansionBedroom bedroom;

    public string Name
    {
        get => name;
        set => name = value;
    }

    public string DisplayName
    {
        get => displayName;
        set => displayName = value;
    }

    public VRCR_MansionBedroom Bedroom
    {
        get => bedroom;
        set => bedroom = value;
    }
}