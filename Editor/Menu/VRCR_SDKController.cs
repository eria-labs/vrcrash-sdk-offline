﻿using UnityEditor;
using UnityEngine;

public class VRCR_SDKController : EditorWindow
{
    private static VRCR_ControlPanelMenu ControlPanelMenu { get; set; }
    private static VRCR_InformationMenu TutorialMenu { get; set; }

    [MenuItem(VRCR_MenuItemsNaming.ControlPanelMenu)]
    private static void ControlPanelWindow()
    {
        ControlPanelMenu = GetWindow<VRCR_ControlPanelMenu>(VRCR_MenuItemsNaming.ControlPanel);
        ControlPanelMenu.maxSize = new Vector2(500, 700);
        ControlPanelMenu.minSize = ControlPanelMenu.maxSize;
    }

    [MenuItem(VRCR_MenuItemsNaming.TutorialMenu)]
    private static void TutorialWindow()
    {
        TutorialMenu = GetWindow<VRCR_InformationMenu>(true, VRCR_MenuItemsNaming.Tutorial);
        TutorialMenu.maxSize = new Vector2(450, 700);
        TutorialMenu.minSize = TutorialMenu.maxSize;
    }
}