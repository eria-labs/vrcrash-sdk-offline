﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCR_ControlPanelMenuStyles : MonoBehaviour
{
    public static GUIStyle AplicationIsPlayingMenuStyle()
    {
        var aplicationIsPlaying = new GUIStyle();
        aplicationIsPlaying.richText = true;
        return aplicationIsPlaying;
    }
}
