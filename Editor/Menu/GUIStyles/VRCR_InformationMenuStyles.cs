﻿using UnityEngine;

public class VRCR_InformationMenuStyles
{


    public static GUIStyle AboutUsStyle()
    {
        var aboutUsStyle = new GUIStyle();
        aboutUsStyle.richText = true;
        var scrollbarStyle = new GUIStyle(GUI.skin.horizontalScrollbar);
        aboutUsStyle.fixedHeight = scrollbarStyle.fixedWidth = 0;
        return aboutUsStyle;
    }

    public static GUIStyle TitleStyle()
    {
        var titleStyle = new GUIStyle();
        titleStyle.fontSize = 24;
        titleStyle.fontStyle = FontStyle.Bold;
        titleStyle.alignment = TextAnchor.MiddleCenter;
        return titleStyle;
    }

    public static GUIStyle HowToStyle()
    {
        var howToSyle = new GUIStyle();
        howToSyle.fontSize = 11;
        howToSyle.padding = new RectOffset(4,4,0,10);
        howToSyle.clipping = TextClipping.Overflow;
        howToSyle.fontStyle = FontStyle.Normal;
        return howToSyle;
    }
    
    public static GUIStyle FAQ_QuestionStyle()
    {
        var questionsStyle = new GUIStyle();
        questionsStyle.fontSize = 12;
        questionsStyle.padding = new RectOffset(4,4,0,0);
        questionsStyle.clipping = TextClipping.Overflow;
        questionsStyle.fontStyle = FontStyle.Bold;
        return questionsStyle;
    }
    
    public static GUIStyle FAQ_AnswersStyle()
    {
        var answersStyle = new GUIStyle();
        answersStyle.fontSize = 11;
        answersStyle.padding = new RectOffset(8,4,0,10);
        answersStyle.clipping = TextClipping.Overflow;
        answersStyle.fontStyle = FontStyle.Normal;
        return answersStyle;
    }
}