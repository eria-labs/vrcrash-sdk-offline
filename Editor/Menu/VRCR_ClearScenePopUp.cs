﻿using System;
using UnityEditor;
using UnityEngine;

public class VRCR_ClearScenePopUp : EditorWindow
{
    private void OnEnable()
    {
        EditorWindow window = GetWindow<VRCR_ClearScenePopUp>("Generar plantilla habitación");
    }

    private void OnGUI()
    {
        EditorGUILayout.HelpBox("Vas a eliminar todo lo que esté en la escena actual, ¿Estás seguro?",
            MessageType.Warning);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Si")) CallClearSceneEvent(true);
        if (GUILayout.Button("No")) CallClearSceneEvent(false);
        GUILayout.EndHorizontal();
    }

    public event EventHandler<ClearSceneEventArgs> ClearSceneEvent;

    protected virtual void OnClearSceneEvent(ClearSceneEventArgs e)
    {
        Close();
        var handler = ClearSceneEvent;
        handler?.Invoke(this, e);
    }

    private void CallClearSceneEvent(bool toClear)
    {
        var args = new ClearSceneEventArgs();
        args.ToClearScene = toClear;
        OnClearSceneEvent(args);
    }

    public class ClearSceneEventArgs : EventArgs
    {
        public bool ToClearScene { get; set; }
    }
}