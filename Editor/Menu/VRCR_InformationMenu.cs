﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
using VRCrash.Editor.Naming;

public class VRCR_InformationMenu : EditorWindow
{
    private delegate void InformationMenu();

    private InformationMenu TitleArea { get; set; }
    private InformationMenu BannerArea { get; set; }
    private InformationMenu Buttons { get; set; }
    private InformationMenu InformationMainSection { get; set; }

    private static Vector2 howToScroll;
    private Texture Banner { get; set; }
    private VRCR_ResourceManagment ResourceManagment { get; set; }


    private void OnEnable()
    {
        ResourceManagment = new VRCR_ResourceManagment();
        Banner = ResourceManagment.LoadSDKBanner();
        BannerArea = BannerShow();
        TitleArea = TitleHowToShow();
        InformationMainSection = TutorialHowToShow();
        Buttons = ButtonsShow();
    }

    private void OnGUI()
    {
        BannerArea();
        TitleArea();
        Buttons();
        InformationMainSection();
    }

    private InformationMenu TitleHowToShow() => delegate
    {
        EditorGUILayout.LabelField("¿Cómo subir habitación?", VRCR_InformationMenuStyles.TitleStyle());
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    };

    private InformationMenu TitleFAQShow() => delegate
    {
        EditorGUILayout.LabelField("Preguntas frecuentes", VRCR_InformationMenuStyles.TitleStyle());
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    };

    private InformationMenu TitleRestrictionsShow() => delegate
    {
        EditorGUILayout.LabelField("Restricciones", VRCR_InformationMenuStyles.TitleStyle());
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    };

    private InformationMenu TutorialHowToShow() => delegate
    {
        howToScroll = GUILayout.BeginScrollView(howToScroll, false, false, GUIStyle.none, GUI.skin.verticalScrollbar,
            GUILayout.ExpandWidth(false));
        GUILayout.Label(VRCR_MessagesNaming.InformationHowTo, VRCR_InformationMenuStyles.HowToStyle());
        GUILayout.EndScrollView();
    };

    private InformationMenu FAQShow() => delegate
    {
        howToScroll = GUILayout.BeginScrollView(howToScroll, false, true, GUIStyle.none, GUI.skin.verticalScrollbar,
            GUILayout.ExpandWidth(false));
        List<string> questions = VRCR_MessagesNaming.GetInformationFAQ_Questions();
        List<string> answers = VRCR_MessagesNaming.GetInformationFAQ_Answers();
        for (var i = 0; i < questions.Count; i++)
        {
            EditorGUILayout.BeginVertical();
            GUILayout.Label(questions[i], VRCR_InformationMenuStyles.FAQ_QuestionStyle());
            GUILayout.Label(answers[i], VRCR_InformationMenuStyles.FAQ_AnswersStyle());
            EditorGUILayout.EndVertical();
        }
        GUILayout.EndScrollView();
    };

    private InformationMenu RestrictionsShow() => delegate
    {
        howToScroll = GUILayout.BeginScrollView(howToScroll, false, true, GUIStyle.none, GUI.skin.verticalScrollbar,
            GUILayout.ExpandWidth(false));
        GUILayout.Label(VRCR_MessagesNaming.Restrictions_1, VRCR_InformationMenuStyles.HowToStyle());
        GUILine();
        GUILayout.Label(VRCR_MessagesNaming.Restrictions_2, VRCR_InformationMenuStyles.HowToStyle());
        GUILayout.EndScrollView();
    };

    private InformationMenu TitleAboutUsShow() => delegate
    {
        EditorGUILayout.LabelField("¿Quienes somos?", VRCR_InformationMenuStyles.TitleStyle());
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    };
    private InformationMenu AboutUsShow() => delegate
    {
       
        howToScroll = GUILayout.BeginScrollView(howToScroll, false, true, GUIStyle.none, GUI.skin.verticalScrollbar,
            GUILayout.ExpandWidth(false));

        if (GUI.Button(new Rect(15, 90, 60, 60), new GUIContent(ResourceManagment.LoadTwitterImage()), GUIStyle.none))
        Application.OpenURL("https://twitter.com/VRCrash");
        
        if (GUI.Button(new Rect(100, 90, 60, 60), new GUIContent(ResourceManagment.LoadYoutubeImage()), GUIStyle.none))
        Application.OpenURL("https://www.youtube.com/c/VRCrash/videos");

        for (int i = 0; i < 4; i++) GUI.Button(new Rect(0, 220 + (i*31), 20, 20), new GUIContent(ResourceManagment.LoadDiscordImage()), GUIStyle.none);

        GUILayout.Label(VRCR_MessagesNaming.AboutUs, VRCR_InformationMenuStyles.HowToStyle());
        GUILayout.EndScrollView();
    };

    private InformationMenu ButtonsShow() => delegate
    {
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("¿Cómo subir habitación?"))
        {
            TitleArea = TitleHowToShow();
            InformationMainSection = TutorialHowToShow();
        }

        if (GUILayout.Button("Restricciones"))
        {
            TitleArea = TitleRestrictionsShow();
            InformationMainSection = RestrictionsShow();
        }
        
        if (GUILayout.Button("FAQ"))
        {
            TitleArea = TitleFAQShow();
            InformationMainSection = FAQShow();
        }
        if (GUILayout.Button("About Us"))
        {
            TitleArea = TitleAboutUsShow();
            InformationMainSection = AboutUsShow();
        }

        if (!VRCR_EasterEgg.isClipPlaying(ResourceManagment.LoadGenKeyMusic()) &&
        GUILayout.Button(new GUIContent(ResourceManagment.LoadPlayImage()), GUIStyle.none))
        {
            PlayGenKeyAudio();

        }else if (VRCR_EasterEgg.isClipPlaying(ResourceManagment.LoadGenKeyMusic()) &&
        GUILayout.Button(new GUIContent(ResourceManagment.LoadPauseImage()), GUIStyle.none))
        {
            StopGenKeyAudio();
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    };


    private void PlayGenKeyAudio()
    {
        VRCR_EasterEgg.PlayClip(ResourceManagment.LoadGenKeyMusic());
    }
    private void StopGenKeyAudio()
    {
        VRCR_EasterEgg.StopClip(ResourceManagment.LoadGenKeyMusic());
    }

    private InformationMenu BannerShow() => delegate
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        if (!(Banner is null))
            GUILayout.Box(Banner, GUILayout.Width(Banner.width * .42f), GUILayout.Height(Banner.height * .42f));
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    };

    void GUILine(int iHeight = 1)
    {
        Rect rect = EditorGUILayout.GetControlRect(false, iHeight);
        rect.height = iHeight;
        EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
    }
}