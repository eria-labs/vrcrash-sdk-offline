﻿using System;
using UnityEditor;
using UnityEngine;

public class VRCR_GnomePopUp : EditorWindow
{
    private Texture2D Banner { get; set; }
    private VRCR_ResourceManagment ResourceManagment { get; set; }

    private void OnEnable()
    {
        ResourceManagment = new VRCR_ResourceManagment();
        Banner = ResourceManagment.LoadGnomeImage();
        VRCR_EasterEgg.PlayClip(ResourceManagment.LoadGnomeAudio());
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Cerrar!")) Close();
        EditorGUILayout.LabelField("You have been gnomed! Con cariño DavidMIX :)", EditorStyles.label);
        GUILayout.Box(Banner, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
    }
}