﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using VRCrash.Editor.Naming;
using Object = UnityEngine.Object;

public class VRCR_ControlPanelMenu : EditorWindow
{
    private bool isUserLogged;
    private int easterEgg;
    private Vector2 scrollPos = Vector2.right;
    private MenuDelegate Menu { get; set; }
    private EasterEggDelegate EasterEgg { get; set; }
    private UserLabelDelegate UserLabel { get; set; }
    private GeneratePackageButtonDelegate GeneratePackage { get; set; }
    private VersionDelegate Version { get; set; }
    private ExitButtonDelegate ExitButton { get; set; }
    private LogOutButtonDelegate LogOutButton { get; set; }
    private CreateFoldersDelegate CreateFoldersButtons { get; set; }
    private VRCR_ResourceManagment ResourceManagment { get; set; }
    private VRCR_Validator Validator { get; set; }
    private static VRCR_ClearScenePopUp ClearScenePopUp { get; set; }
    private VRCR_User CurrentUser { get; set; }
    private Texture Banner { get; set; }
    private VRCR_Users Users { get; set; }
    private GameObject BedroomPrefab { get; set; }
    private GameObject TemplateGameObject { get; set; }
    private string Username { get; set; }
    private bool awaitRuning = false;

    private void OnEnable()
    {
        Application.runInBackground = true;
        easterEgg = 0;
        UserLabel = EmptyUserLabel();
        Version = DisplayVersion();
        CreateFoldersButtons = delegate { };
        GeneratePackage = GeneratePackageButtonHide();
        ResourceManagment = new VRCR_ResourceManagment();
        Validator = new VRCR_Validator();
        Users = ResourceManagment.ReadJsonUsers();
        Banner = ResourceManagment.LoadSDKBanner();
        EasterEgg = EasterEggShow();
        LoadSessionState();
        try
        {
            CurrentUser = Users.users.First(user => user.Name.Equals(Username));
        }
        catch (Exception e)
        {
            e.Message.ToString();// to supress warning
            //Debug.Log(e.Message);
        }
    }

    private void OnDestroy()
    {
        SaveSessionState();
    }
    private void OnGUI()
    {
        CheckSceneStep();
        if (Users is null) Users = ResourceManagment.ReadJsonUsers();
        if (Banner is null) Banner = ResourceManagment.LoadSDKBanner();
        if (isUserLogged) UserLabel = UserInformation();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        if (!(Banner is null))
            GUILayout.Box(Banner, GUILayout.Width(Banner.width * .45f), GUILayout.Height(Banner.height * .45f));
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        UserLabel();
        GeneratePackage();
        GUILayout.EndHorizontal();
        Menu();
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        CreateFoldersButtons();
        LogOutButton();
        ExitButton();
        Version();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        EasterEgg();
    }

    private VersionDelegate DisplayVersion()
    {
        return delegate
        {
            var versionStyle = new GUIStyle();
            versionStyle.fontSize = 10;
            versionStyle.padding = new RectOffset(0, 10, 0, 5);
            versionStyle.font = Font.CreateDynamicFontFromOSFont(VRCR_ConstLiteralsNaming.RobotoFont, 10);
            versionStyle.fontStyle = FontStyle.Italic;
            versionStyle.alignment = TextAnchor.LowerRight;
            EditorGUILayout.LabelField(AppInfo.Version, versionStyle, GUILayout.MaxHeight(position.height),
                GUILayout.Width(position.width));
        };
    }

    private UserLabelDelegate EmptyUserLabel() => delegate { };
    private CreateFoldersDelegate CreateFoldesrHide() => delegate { };

    private CreateFoldersDelegate CreateFoldersShow() => delegate
    {
        var isValidPrefabs = AssetDatabase.IsValidFolder("Assets/VRCrash/Prefabs");
        var isValidUnityPackages = AssetDatabase.IsValidFolder("Assets/VRCrash/UnityPackages");
        
        if (!isValidPrefabs
            && GUILayout.Button("Crear carpetas Prefabs"))
            AssetDatabase.CreateFolder("Assets/VRCrash", "Prefabs");
        if (!isValidUnityPackages
            && GUILayout.Button("Crear carpetas UnityPackages"))
            AssetDatabase.CreateFolder("Assets/VRCrash", "UnityPackages");
        if (isValidPrefabs && isValidUnityPackages) CreateFoldersButtons = CreateFoldesrHide();
    };

    private UserLabelDelegate UserInformation()
    {
        return delegate
        {
            var sessionStyle = new GUIStyle();
            sessionStyle.font = Font.CreateDynamicFontFromOSFont(VRCR_ConstLiteralsNaming.RobotoFont, 12);
            sessionStyle.alignment = TextAnchor.MiddleLeft;
            var bedroomInfoStyle = new GUIStyle();
            bedroomInfoStyle.font = Font.CreateDynamicFontFromOSFont(VRCR_ConstLiteralsNaming.RobotoFont, 10);
            bedroomInfoStyle.alignment = TextAnchor.MiddleLeft;
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Sesión iniciada como: " + CurrentUser.displayName, sessionStyle);
            EditorGUILayout.LabelField("Habitación: " + CurrentUser.Bedroom.number, bedroomInfoStyle);
            string version = ResourceManagment.SearchBedroomVersion().ToString();
            if (version == "-1")
            {
                CreateFoldersButtons = CreateFoldersShow();
                version = "error, los directorios Prefabs y UnityPackages no existen";
            }
            EditorGUILayout.LabelField("Versión de la habitación: " + version,
                bedroomInfoStyle);
            EditorGUILayout.Separator();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        };
    }

    private MenuDelegate ClearSceneStep()
    {
        return delegate
        {
            GUILayout.Label(VRCR_MessagesNaming.PreparingScene, EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("¡CUIDADO! Se va a ELIMINAR todo de la escena actual ¡CUIDADO!",
                MessageType.Warning);
            if (GUILayout.Button(VRCR_MessagesNaming.GenerateTemplate)) CreateClearScenePopUpWindow();
        };
    }

    private EasterEggDelegate EasterEggShow() => delegate
    {
        if (GUI.Button(new Rect(0, 0, 450, 150), "", GUIStyle.none))
        {
            easterEgg++;
            if (easterEgg == 6) CreateGnomePopUpWindow();
        }
    };

    private MenuDelegate ClearSceneStepWithWrongRoomType()
    {
        return delegate
        {
            GUILayout.Label(VRCR_MessagesNaming.PreparingScene, EditorStyles.boldLabel);
            EditorGUILayout.HelpBox(
                "Este tipo de habitación no se corresponde con el que tienes asignado, por favor haz una copia de seguridad de todos los cambios que hayas hecho y crea de nuevo el template",
                MessageType.Error);
            EditorGUILayout.HelpBox("¡CUIDADO! Se va a ELIMINAR todo de la escena actual ¡CUIDADO!",
                MessageType.Warning);
            if (GUILayout.Button(VRCR_MessagesNaming.GenerateTemplate)) CreateClearScenePopUpWindow();
        };
    }

    private MenuDelegate SigningInFailedStep() => delegate 
    {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox(VRCR_MessagesNaming.UserNotFound + Username + VRCR_ConstLiteralsNaming.Dot,
            MessageType.Error);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    };

    private async Task runAwait(int time)
    {
        awaitRuning = true;
        await Task.Delay(time);
        awaitRuning = false;
        Repaint();
    }

    private MenuDelegate CreationStep()
    {
        return delegate
        {
            try
            {
                EditorGUILayout.BeginVertical(GUILayout.Height(position.height - Banner.height * .45f - 188));
                scrollPos = GUILayout.BeginScrollView(scrollPos, false, false, GUIStyle.none,
                    GUI.skin.verticalScrollbar,
                    GUILayout.ExpandWidth(true));
                GeneratePackage = GeneratePackageButtonDisable();
                if (Validator.ValidateRestrictions(TemplateGameObject)) GeneratePackage = GeneratePackageButtonEnable();
            }
            catch (MissingReferenceException e)
            {
                e.Message.ToString(); // to supress warning
                BedroomPrefab = null;
                TemplateGameObject = null;
                Menu = ClearSceneStep();
            }
            catch (NullReferenceException e)
            {
                e.Message.ToString(); // to supress warning
                TemplateGameObject = null;
                Menu = ClearSceneStep();
                isUserLogged = true;
            }
            finally
            {
                EditorGUILayout.EndScrollView();
                EditorGUILayout.EndVertical();
            }
        };
    }

    private MenuDelegate AplicationIsPlayingMenu() => delegate
    {
        GUILayout.Label(VRCR_MessagesNaming.ErrorAplicationIsPlaying, VRCR_InformationMenuStyles.HowToStyle());
    };

    private MenuDelegate SignInStep()
    {
        return delegate
        {
            
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical(VRCR_MessagesNaming.LogIn, VRCR_ConstLiteralsNaming.Window, GUILayout.Height(54),
                GUILayout.Width(450));
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            Username = EditorGUILayout.TextField(VRCR_MessagesNaming.Username, Username);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
            GUILayout.Space(2);
            if (GUILayout.Button(VRCR_MessagesNaming.Access)) LogIn(Username);
            GUILayout.Space(2);
            if (GUILayout.Button(VRCR_MessagesNaming.ExitWindow)) Close();
            EditorGUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        };
    }

    private MenuDelegate SigningInStep()
    {
        return delegate
        {

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Logeándote como " + Username + VRCR_ConstLiteralsNaming.ThreeDots);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        };
    }

    private LogOutButtonDelegate LogOutButtonEnable()
    {
        return delegate
        {
            if (GUILayout.Button(VRCR_MessagesNaming.ChangeUser, GUILayout.Width(493)))
            {
                isUserLogged = false;
                CurrentUser = null;
                UserLabel = EmptyUserLabel();
                GeneratePackage = GeneratePackageButtonHide();
            }
        };
    }


    private LogOutButtonDelegate LogOutButtonDisable()
    {
        return delegate { };
    }

    private ExitButtonDelegate ExitButtonEnable()
    {
        return delegate
        {
            if (GUILayout.Button(VRCR_MessagesNaming.ExitWindow, GUILayout.Width(493))) Close();
        };
    }

    private ExitButtonDelegate ExitButtonDisable()
    {
        return delegate { };
    }

    private GeneratePackageButtonDelegate GeneratePackageButtonEnable()
    {
        return delegate
        {
            try
            {
                if (GUILayout.Button("Generar paquete de la habitación")) GenerateBedroomPackage();
            }
            catch (ArgumentException e)
            {
                e.Message.ToString(); // to supress warning
            }
        };
    }

    private GeneratePackageButtonDelegate GeneratePackageButtonDisable()
    {
        return delegate
        {
            try
            {
                GUI.enabled = false;
                GUILayout.Button("Generar paquete de la habitación");
                GUI.enabled = true;
            }
            catch (ArgumentException e)
            {
                e.Message.ToString(); // to supress warning
            }
        };
    }

    private GeneratePackageButtonDelegate GeneratePackageButtonHide()
    {
        return delegate { };
    }

    private async void LogIn(string username)
    {
        try
        {
            CurrentUser = Users.users.First(user => user.Name.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            Menu = SigningInStep();
            await runAwait(1000);
            Menu = ClearSceneStep();
            isUserLogged = true;


        }
        catch (Exception e)
        {
            e.Message.ToString(); // to supress warning          
            Menu = SigningInFailedStep();
            await runAwait(1000);
        }
    }

    private void CreateClearScenePopUpWindow()
    {
        ClearScenePopUp = GetWindow<VRCR_ClearScenePopUp>("Generar plantilla habitación");
        ClearScenePopUp.maxSize = new Vector2(300, 200);
        ClearScenePopUp.minSize = ClearScenePopUp.maxSize;
        ClearScenePopUp.ClearSceneEvent += ClearSceneFromPopUpAndGenerateBedroomTemplate;
        isUserLogged = true;
    }

    private void CreateGnomePopUpWindow()
    {
        VRCR_GnomePopUp gnomePopUp = GetWindow<VRCR_GnomePopUp>("You have been gnomed");
        gnomePopUp.position = new Rect(Screen.width / 2, Screen.height / 2, 500, 500);
        gnomePopUp.ShowPopup();
    }

    private void ClearSceneFromPopUpAndGenerateBedroomTemplate(object sender,
        VRCR_ClearScenePopUp.ClearSceneEventArgs e)
    {
        if (e.ToClearScene) ClearScene();
        InstantiateBedroomTemplate();
        ClearScenePopUp.ClearSceneEvent -= ClearSceneFromPopUpAndGenerateBedroomTemplate;
    }

    private void InstantiateBedroomTemplate()
    {
        var roomPrefab = ResourceManagment.LoadBedroomPrefab(CurrentUser);
        BedroomPrefab = (GameObject) PrefabUtility.InstantiatePrefab(roomPrefab);
        TemplateGameObject = Validator.FindGameObjectByTag(VRCR_MessagesNaming.TagTemplate);
        Menu = TemplateGameObject is null ? ClearSceneStep() : CreationStep();
    }

    private void ClearScene()
    {
        object[] obj = FindObjectsOfType(typeof(GameObject));
        foreach (var o in obj)
        {
            var g = (GameObject) o;
            if (g != null && !g.name.Equals(VRCR_RestrictionsNaming.DirectionalLight))
            {
                if (PrefabUtility.IsPartOfPrefabInstance(g.transform))
                {
                    var root = PrefabUtility.GetOutermostPrefabInstanceRoot(g);
                    PrefabUtility.UnpackPrefabInstance(root,
                        PrefabUnpackMode.Completely,
                        InteractionMode.AutomatedAction
                    );
                    DestroyImmediate(root);
                }
                else
                {
                    DestroyImmediate(g);
                }
            }
        }
    }

    private void CheckSceneStep()
    {
        ExitButton = ExitButtonEnable();
        LogOutButton = LogOutButtonEnable();
        if (Application.isPlaying)
        {
            ExitButton = ExitButtonDisable();
            LogOutButton = LogOutButtonDisable();
            Menu = AplicationIsPlayingMenu();
            return;
        }
        if (CurrentUser is null || !isUserLogged)
        {
            ExitButton = ExitButtonDisable();
            LogOutButton = LogOutButtonDisable();
            if (!awaitRuning)
            Menu =  SignInStep();
            return;
        }

        try
        {
            if (BedroomPrefab is null || !BedroomPrefab.CompareTag(VRCR_MessagesNaming.TagBedroomType1) &&
                !BedroomPrefab.CompareTag(VRCR_MessagesNaming.TagBedroomType2))
            {
                BedroomPrefab = Validator.FindGameObjectByTag(VRCR_MessagesNaming.TagBedroomType1) ??
                                Validator.FindGameObjectByTag(VRCR_MessagesNaming.TagBedroomType2);
                if (BedroomPrefab is null)
                {
                    Menu = ClearSceneStep();
                    return;
                }
            }

            if (TemplateGameObject is null || !TemplateGameObject.CompareTag(VRCR_MessagesNaming.TagTemplate))
            {
                TemplateGameObject = Validator.FindGameObjectByTag(VRCR_MessagesNaming.TagTemplate);
                if (TemplateGameObject is null)
                {
                    Menu = ClearSceneStep();
                    return;
                }
            }
        }
        catch (MissingReferenceException e)
        {
            e.Message.ToString(); // to supress warning
            BedroomPrefab = null;
            TemplateGameObject = null;
            Menu = ClearSceneStep();
            return;
        }
        catch (NullReferenceException e)
        {
            e.Message.ToString(); // to supress warning
            TemplateGameObject = null;
            Menu = ClearSceneStep();
            return;
        }

        if (!BedroomPrefab.CompareTag(CurrentUser.Bedroom.GetBedroomTypeTag()))
        {
            Menu = ClearSceneStepWithWrongRoomType();
            GeneratePackage = GeneratePackageButtonHide();
            return;
        }

        Menu = CreationStep();
    }

    private GameObject FindGameObjectByInstanceID(int instanceId)
    {
        if (instanceId == 0) return null;
        foreach (var obj in FindObjectsOfType(typeof(Object)))
            if (obj.GetInstanceID() == instanceId)
                return (GameObject) obj;

        return null;
    }

    private void GenerateBedroomPackage()
    {
        string[] folders = {"Assets/VRCrash/Prefabs", "Assets/VRCrash/UnityPackages"};
        if (!ResourceManagment.AreFoldersValid(folders))
        {
            Debug.LogError("No se encontró el directorio de VRCrash/Prefabs o VRCrash/UnityPackages");
            return;
        }

        ResourceManagment.GenerateVersionedBedroomUnityPackage(BedroomPrefab, CurrentUser.Bedroom.Number,
            ResourceManagment.SearchBedroomVersion());
    }

    private void LoadSessionState()
    {
        isUserLogged = SessionState.GetBool(VRCR_SessionStateNaming.IsUserLoggedVar, false);
        Username = SessionState.GetString(VRCR_SessionStateNaming.currentUserNameVar, null);
        BedroomPrefab =
            FindGameObjectByInstanceID(SessionState.GetInt(VRCR_SessionStateNaming.BedroomInstanceIDVar, 0));
        TemplateGameObject =
            FindGameObjectByInstanceID(SessionState.GetInt(VRCR_SessionStateNaming.TemplateInstanceIDVar, 0));
    }

    private void SaveSessionState()
    {
        SessionState.SetBool(VRCR_SessionStateNaming.IsUserLoggedVar, isUserLogged);
        if (!(CurrentUser is null))
            SessionState.SetString(VRCR_SessionStateNaming.currentUserNameVar, CurrentUser.Name);

        if (!(BedroomPrefab is null))
            SessionState.SetInt(VRCR_SessionStateNaming.BedroomInstanceIDVar, BedroomPrefab.GetInstanceID());

        if (!(TemplateGameObject is null))
            SessionState.SetInt(VRCR_SessionStateNaming.TemplateInstanceIDVar, TemplateGameObject.GetInstanceID());
    }

    private delegate void MenuDelegate();

    private delegate void EasterEggDelegate();

    private delegate void UserLabelDelegate();

    private delegate void GeneratePackageButtonDelegate();

    private delegate void VersionDelegate();

    private delegate void ExitButtonDelegate();

    private delegate void LogOutButtonDelegate();

    private delegate void CreateFoldersDelegate();
}