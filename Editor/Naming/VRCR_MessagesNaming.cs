﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace VRCrash.Editor.Naming
{
    public class VRCR_MessagesNaming
    {
        // Messages
        public const string UserNotFound = "No existe el usuario ";
        public const string PreparingScene = "Preparando escena";

        // TextField
        public const string Username = "Nombre de usuario";

        // Buttons
        public const string GenerateTemplate = "Generar template";
        public const string LogIn = "Log In";
        public const string Access = "Acceder";
        public const string ChangeUser = "Cambiar usuario";
        public const string ExitWindow = "Salir";

        // Tags
        public const string TagTemplate = "TemplateGO";
        public const string TagBedroomType1 = "BedroomPrefabType1";
        public const string TagBedroomType2 = "BedroomPrefabType2";


        // Mucho texto
        public const string InformationHowTo = @"
1. Instalar Unity (recomendada versión 2018.4.20f1), puedes
descargarlo desde Unity Hub.

2. Crear en Unity un proyecto 3D.

3. Debes importar el SDK en tu proyecto de Unity arrastrándolo.

4. Aparecerá una nueva pestaña en el menú superior que ponga VRCrash.

5. Dentro del menú seleccionar la opción Panel de Control.

6. Introducir tu nombre de usuario de VRChat y pulsar el botón
generar plantilla habitación. Se eliminará todo lo de la escena por
completo CUIDADO CON ESTE PASO.

7. Ahora puedes editar todo lo de la habitación metiendo los objetos
que quieras en la sección [Pon tus modelos aqui dentro].

8. Según introduzcas nuevos 'GameObjects' el SDK irá validando si
cumplen las restricciones. Aparecerán indicadas en el SDK.

9. Una vez termineis de editar la habitación y cumpla los requisitos
aparecerá una opción para generar la habitación final. Esto generará
un unitypackage (está en la carpeta UnityPackages dentro de
Resources).

10. Puedes generar cuantas habitaciones quieras, mientras mantengas
el mismo proyecto se generará un nuevo unitypackge con un nuevo
número de versión.

11. Una vez estéis contentos con la habitación nos paseis el último
UnityPackage que habéis generado.

Informar de que además de esto, nosotros validaremos que la
habitación cumpla unos mínimos. Por ejemplo, si alguien coloca un
cuadro y vemos que está volando preguntaremos si era la intención
sino ayudaremos a corregir este tipo de detalles. De todas formas no
dudéis en preguntar en cualquier momento, solemos estar por discord e
intentaremos ayudar con lo que sea.";

        public const string FAQ_QUESTION_1 = @"
1. ¿Si hay otros objetos en la escena se deben de eliminar?";

        public const string FAQ_ANSWER_1 = @"
No es necesario eliminarlos, el paquete solo se genera del GameObject
de la habitación. Sin embargo, al generar el template se elimina todo
lo de la escena.";

        public const string FAQ_QUESTION_2 = @"
2. ¿Cuales son los requisitos?";

        public const string FAQ_ANSWER_2 = @"
Mirar la sección de Requisitos.";

        public const string FAQ_QUESTION_3 = @"
3. ¿Qué hago si quiero ir cambiando mi habitación antes de la subida
de la mansión?";

        public const string FAQ_ANSWER_3 = @"
Mientras se mantengan los nombres de la habitación por defecto, cada
versión de tu habitación pondrá automáticamente el número de versión
actual.";

        public const string FAQ_QUESTION_4 = @"
4. ¿Hay que guardar las texturas en algún sitio en específico?";

        public const string FAQ_ANSWER_4 = @"
Al presionar el botón de 'Generar paquete de la habitación' ya se
incluyen las dependencias. De todas formas, es importantísimo guardar
y organizar las texturas por si hubiese algún error.";

        public const string FAQ_QUESTION_5 = @"
5. ¿Qué hago si tengo otro shader que no es el 'Standard'?";

        public const string FAQ_ANSWER_5 = @"
Contanta con nosotros y valoraremos incluirlo.";

        public const string FAQ_QUESTION_6 = @"
6. ¿Qué pasa si tengo un objecto fuera de la habitación?";

        public const string FAQ_ANSWER_6 = @"
Lo borraremos automáticamente.";

        public const string FAQ_QUESTION_7 = @"
7. ¿La habitación que se genere con el botón es la que estará en la
mansión?";

        public const string FAQ_ANSWER_7 = @"
No necesariamente, la habitación pasa 2 validaciones. Una automática
del SDK y una manual en la cual se valorará con otros criterios si
la habitación es correcta.";

        public const string FAQ_QUESTION_8 = @"
8. ¿Por qué no se puede poner más de una luz?";

        public const string FAQ_ANSWER_8 = @"
Porque penaliza el rendimiento y aún no sabemos como haremos el tema
de la iluminación.";

        public const string FAQ_QUESTION_9 = @"
9. ¿Por qué no puedo promocionar mi contenido en mi
habitación?";

        public const string FAQ_ANSWER_9 = @"
Varios motivos. Uno es que el mapa tiene el objetivo de ser la casa
de VRCrash y en nuestra casa no ponemos publicidad. Segundo, porque
no vamos a validar ninguna fuente de lo que habeis creado, si lo que
promocionais es de otra persona, si el contenido es explícito,
etc etc... Nosotros no queremos tener nada que ver con ello.

Para promocionar algo haremos una sección en la mansión, se pondrá
un anuncio para los que quieran promocionar algo puedan contactar
con nosotros y le diremos que está permitido.";

        public const string FAQ_QUESTION_10 = @"
10. ¿Se puede sobrepasar alguna de las restricciones?";

        public const string FAQ_ANSWER_10 = @"
En la mayoría de casos no, pero existe cierta flexibilidad.
En caso de que quieras sobrepasar alguna de ellas contacta
con nosotros y analizaremos si es posible.";

        public const string FAQ_QUESTION_11 = @"
11. ¿Por qué hay dos prefabs de la habitación?";

        public const string FAQ_ANSWER_11 = @"
Porque existen dos tipos de habitaciones según la zona de la mansión
en la que estén. No se puede cambiar el tipo salvo casos
excepcionales.";

        public const string FAQ_QUESTION_12 = @"
12. ¿Qué pasa si tengo habitación compartida?";

        public const string FAQ_ANSWER_12 = @"
Poneros de acuerdo ya que cualquiera de los dos puede hacer la
habitación pero solo el dueño puede subirla.";

        public const string FAQ_QUESTION_13 = @"
13. ¿Qué texturas puedo editar?";

        public const string FAQ_ANSWER_13 = @"
Hay una carpeta llamada Resources -> Texturas y ahí dentro aparecerán
todas las texturas por defecto. La textura de la puerta no será la
definitiva y aunque la edites no estará en tu habitación ya que todas
las habitaciones tendrán las mismas puertas. El resto son totalmente
editables. Puedes añadir nuevas texturas pero las paredes exterior y
el suelo de la entrada no se tendrán en cuenta.

También se han incluído los PSDs de las texturas para facilitar la
edición. Además se han incluído las imágenes de los UVs por si os
sirven de algo.

Aclaración: La puerta, la ventana, la pared de fuera de la
habitación y el suelo de fuera de la habitación no son editables.";

        public const string FAQ_QUESTION_14 = @"
14. ¿Qué resolución de las texturas debo usar?";

        public const string FAQ_ANSWER_14 = @"
1024x1024. Si usáis una resolución mayor tener en cuenta que la
bajaremos a esa.";

        public const string FAQ_QUESTION_15 = @"
15. Las paredes interior tienen unos salientes arriba y abajo,
¿eso se verá?";

        public const string FAQ_ANSWER_15 = @"
No, eso es para que sea más sencillo de visualizar la habitación,
luego el modelo se optimizará lo máximo posible eliminando cualquier
cara que no se vea, esas incluidas.";

        public const string FAQ_QUESTION_16 = @"
16. ¿Cómo organizar el proyecto?";

        public const string FAQ_ANSWER_16 = @"
Mi recomendación es crear tu propia carpeta y llamarla: Habitación.
Ahí dentro crear una carpeta para:
- Materiales
- Texturas
- Modelos

Y todos lo que uséis para vuestra habitación (aunque sean los
materiales o texturas por defecto) lo mováis/creéis ahí y empecéis
solo editéis lo de vuestra carpeta.";

        public const string FAQ_QUESTION_17 = @"
17. ¿Hay tiempo límite?";

        public const string FAQ_ANSWER_17 = @"
Desde el momento en el que se publique este SDK por primera vez, el
mismo día del mes siguiente. Puedes verlo en la sección de anuncios
del discord.";

        public const string FAQ_QUESTION_18 = @"
18. ¿Qué hago si tengo un error que no entiendo?";

        public const string FAQ_ANSWER_18 = @"
Contacta con los admins en discord.";

        public const string FAQ_QUESTION_19 = @"
19. ¿Qué hago si encuentro un error en el SDK?";

        public const string FAQ_ANSWER_19 = @"
Contacta con los admins en discord para que podamos crear una tarea
para solucionarlo.";


        public const string Restrictions_1 = @"
- Máximo de triángulos: 50.000
- Máximo de materiales: 10
- Máximo de luces: Ninguno, pero se puede usar emisión en los materiales.
- Los materiales solo pueden ser 'Standard', si quieres usar otro shader
  contactar con los admins.
- Los componontes que se pueden usar son: 
    - TransformComponent.
    - MeshFilterComponent.
    - MeshRendererComponent.
    - UdonBehaviour (contactar con los admins).
- No se pueden promocionar ningún contenido.
- No se pueden poner portales a otros mundos.
- Si queréis tener espejos solo debéis preparar el espejo y los botones,
nosotros añadimos la funcionalidad. Si queréis probarlos solo tenéis
que contactar con alguno de los admins y os ayudaremos con el prefab
de Udon para probarlo.";

        public const string Restrictions_2 = @"
Podeis hacer lo que queráis con vuestra escena de Unity pero solo
se tendrá en cuenta los materiales de: 
- Pared interior
- Suelo
- Techo
- Placa
- Lo incluido dentro del GameObject [Pon tus modelos aqui dentro]

Es recomendado usar el SDK3 de mundos de VRChat.";

        public const string AboutUs = @"
Somos una pequeña comunidad del mundo de VR, formada por gente muy
loca que le gusta crear contenido multimedia enfocado en el sector
del VR.

<size=20><b>Redes sociales</b></size>





<size=20><b>Integrantes del proyecto</b></size>
Actualmente el desarrollo del proyecto de la mansión esta siendo
realizado por:

<size=15>   Othou#7944</size>
    <i>'Los tractores son buenos'</i>
<size=15>   Continuum#6082</size>
    <i>'No hay no existo'</i>
<size=15>   Daiya#2323</size>
    <i>'Soy gay'</i>
<size=15>   DavidMIX#5320</size>
    <i>'A ver... que si se puede'</i>

                                                                          <i>this is the awakening</i>
                                                                                            <i>-VRcrash</i>
";

public const string ErrorAplicationIsPlaying = @"
                          <size=20><b><color=#ff0000ff>Sal del modo play de unity 
            para continuar usando el SDK</color></b></size>       


                                                                                                      <i>'Catorce'</i>
                                                                                                         <i>-VRcrash</i>





    ";

        public static List<string> GetInformationFAQ_Questions()
        {
            List<string> questions = new List<string>();
            questions.Add(FAQ_QUESTION_1);
            questions.Add(FAQ_QUESTION_2);
            questions.Add(FAQ_QUESTION_3);
            questions.Add(FAQ_QUESTION_4);
            questions.Add(FAQ_QUESTION_5);
            questions.Add(FAQ_QUESTION_6);
            questions.Add(FAQ_QUESTION_7);
            questions.Add(FAQ_QUESTION_8);
            questions.Add(FAQ_QUESTION_9);
            questions.Add(FAQ_QUESTION_10);
            questions.Add(FAQ_QUESTION_11);
            questions.Add(FAQ_QUESTION_12);
            questions.Add(FAQ_QUESTION_13);
            questions.Add(FAQ_QUESTION_14);
            questions.Add(FAQ_QUESTION_15);
            questions.Add(FAQ_QUESTION_16);
            questions.Add(FAQ_QUESTION_17);
            questions.Add(FAQ_QUESTION_18);
            questions.Add(FAQ_QUESTION_19);
            return questions;
        }

        public static List<string> GetInformationFAQ_Answers()
        {
            List<string> answers = new List<string>();
            answers.Add(FAQ_ANSWER_1);
            answers.Add(FAQ_ANSWER_2);
            answers.Add(FAQ_ANSWER_3);
            answers.Add(FAQ_ANSWER_4);
            answers.Add(FAQ_ANSWER_5);
            answers.Add(FAQ_ANSWER_6);
            answers.Add(FAQ_ANSWER_7);
            answers.Add(FAQ_ANSWER_8);
            answers.Add(FAQ_ANSWER_9);
            answers.Add(FAQ_ANSWER_10);
            answers.Add(FAQ_ANSWER_11);
            answers.Add(FAQ_ANSWER_12);
            answers.Add(FAQ_ANSWER_13);
            answers.Add(FAQ_ANSWER_14);
            answers.Add(FAQ_ANSWER_15);
            answers.Add(FAQ_ANSWER_16);
            answers.Add(FAQ_ANSWER_17);
            answers.Add(FAQ_ANSWER_18);
            answers.Add(FAQ_ANSWER_19);
            return answers;
        }
    }

}