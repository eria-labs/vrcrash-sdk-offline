﻿public class VRCR_RestrictionsNaming
{
    public const string DirectionalLight = "Directional Light";
    public const string DollarCharNotPermitted = "No se permite el uso del caracter $ en el nombre del GameObject.";
    public const string MaximumNumberOfLightsAllowed = "El numero maximo de luces que se permiten son: ";
    public const string Maximum = " \n Maximo: ";
 
    public const string TrianglesNumber = "Número de triangulos: ";
    public const string MaterialsNumber = "Número de materiales: ";
    public const string SurpassedTrianglesMaximum = " has sobrepasado el limite, reduce la cantidad de triángulos. \n Máximo: ";
    public const string SurpassedMaterialsMaximum =
        " has sobrepasado el limite, reduce la cantidad de materiales. \n Máximo: ";

    public const string InvalidShader = "Shader no válido: ";
    public const string InvalidComponents = "Hay componentes inválidos: ";


    private const string LightComponentName = "Light";
    private const string TransformComponentName = "Transform";
    private const string MeshFilterComponentName = "MeshFilter";
    private const string MeshRendererComponentName = "MeshRenderer";
    public static readonly int MAX_TRIANGLES = 50000;
    public static readonly int MAX_MAT = 10;
    public static readonly int MAX_LIGHTS = 1;

    public static readonly string[] VALID_COMPONENTS =
    {
        LightComponentName,
        TransformComponentName,
        MeshFilterComponentName,
        MeshRendererComponentName
    };

    public const string StandarShader = "Standard";
    public const string PoiyomiShaderCutout = ".poiyomi/Toon/Advanced/Cutout";
    public const string PoiyomiShaderOpaque = ".poiyomi/Toon/Advanced/Opaque";
    public const string PoiyomiShaderOutlinesCutout = ".poiyomi/Toon/Advanced/Outlines Cutout";
    public const string PoiyomiShaderOutlinesTransparent = ".poiyomi/Toon/Advanced/Outlines Transparent";
    public const string PoiyomiShaderMasterScanner = ".poiyomi/Toon/Extras/MasterScanner";
    public const string PoiyomiShaderStencilInvis = ".poiyomi/Toon/Extras/StencilInvis";
    public const string PoiyomiShaderTransparent = ".poiyomi/Toon/Advanced/Transparent";

    public const string XiexeXSToon2 = "Xiexe/Toon2.0/XSToon2.0";
    public const string XiexeXSToon2Cutout = "Xiexe/Toon2.0/XSToon2.0_Cutout";
    public const string XiexeXSToon2CutoutA2C = "Xiexe/Toon2.0/XSToon2.0_CutoutA2C";
    public const string XiexeXSToon2CutoutA2CMasked = "Xiexe/Toon2.0/XSToon2.0_CutoutA2C_Masked";
    public const string XiexeXSToon2CutoutA2COutlined = "Xiexe/Toon2.0/XSToon2.0_CutoutA2C_Outlined";
    public const string XiexeXSToon2Dithered = "Xiexe/Toon2.0/XSToon2.0_Dithered";
    public const string XiexeXSToon2DitheredOutlined = "Xiexe/Toon2.0/XSToon2.0_Dithered_Outlined";
    public const string XiexeXSToon2Fade = "Xiexe/Toon2.0/XSToon2.0_Fade";
    public const string XiexeXSToon2Outlined = "Xiexe/Toon2.0/XSToon2.0_Outlined";
    public const string XiexeXSToon2Transparent = "Xiexe/Toon2.0/XSToon2.0_Transparent";
    public const string XiexeXSToon2TransparentShadowFromUnderneathHack = "Xiexe/Toon2.0/XSToon2.0_Transparent_ShadowFromUnderneath_Hack";
    public const string XiexeXSToon2WireframeOverride = "Xiexe/Toon2.0/XSToon2.0_WireframeOverride";
    public const string XiexeXSToon2WireframeOverrideCutoutA2C = "Xiexe/Toon2.0/XSToon2.0_WireframeOverride_CutoutA2C";
    public const string XiexeXSToon2XSToonStenciler = "Xiexe/Toon2.0/XSToonStenciler";

    public static readonly string[] VALID_SHADERS =
    {
        StandarShader,
        PoiyomiShaderCutout,
        PoiyomiShaderOpaque,
        PoiyomiShaderOutlinesCutout,
        PoiyomiShaderOutlinesTransparent,
        PoiyomiShaderMasterScanner,
        PoiyomiShaderStencilInvis,
        PoiyomiShaderTransparent,
        XiexeXSToon2,
        XiexeXSToon2Cutout,
        XiexeXSToon2CutoutA2C,
        XiexeXSToon2CutoutA2CMasked,
        XiexeXSToon2CutoutA2COutlined,
        XiexeXSToon2Dithered,
        XiexeXSToon2DitheredOutlined,
        XiexeXSToon2Fade,
        XiexeXSToon2Outlined,
        XiexeXSToon2Transparent,
        XiexeXSToon2TransparentShadowFromUnderneathHack,
        XiexeXSToon2WireframeOverride,
        XiexeXSToon2WireframeOverrideCutoutA2C,
        XiexeXSToon2XSToonStenciler
    };


}