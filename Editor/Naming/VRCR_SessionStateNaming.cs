﻿namespace VRCrash.Editor.Naming
{
    public class VRCR_SessionStateNaming
    {
        public const string IsUserLoggedVar = "isUserLogged";
        public const string currentUserNameVar = "currentUserName";
        public const string BedroomInstanceIDVar = "BedroomInstanceID";
        public const string TemplateInstanceIDVar = "TemplateInstanceID";
    }
}