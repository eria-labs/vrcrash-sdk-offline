﻿public class VRCR_ResourceManagmentNaming
{
    public const string ResourcesPath = "Assets/VRCrash/Resources/";
    public const string VRCrashPath = "Assets/VRCrash/";
    public const string PrefabsDirectoryName = "Prefabs/";
    public const string UnityPackageDirectoryName = "UnityPackages/";
    public const string Models = "Modelos Base/";
    public const string GnomeAudio = "get-gnomed.mp3";
    public const string GenKeyAudio = "lsg35-dv5sg.ogg";
    public const string GnomeImage = "gnome.png";
    public const string DiscordImage = "Discord-Logo-Color.png";
    public const string TwitterImage = "twitter.png";
    public const string YoutubeImage = "youtube.png";
    public const string PlayImage = "play.png";
    public const string PauseImage = "pausa.png";

    public const string BedroomBaseExportingName = "Bedroom";
    public const string BedroomType1Name = "BedroomType1.prefab";
    public const string BedroomType2Name = "BedroomType2.prefab";
    
    public const string SDKBannerFile = "sdk.png";
    public const string OfflineDBFile = "OfflineDB.json";
    public static readonly string BedroomType1Path = $"{ResourcesPath}{Models}{BedroomType1Name}";
    public static readonly string BedroomType2Path = $"{ResourcesPath}{Models}{BedroomType2Name}";
    public static readonly string GnomeAudioPath = $"{ResourcesPath}{GnomeAudio}";
    public static readonly string GenKeyAudioPath = $"{ResourcesPath}{GenKeyAudio}";
    public static readonly string GnomeImagePath = $"{ResourcesPath}{GnomeImage}";
    public static readonly string SDKBannerPath = $"{ResourcesPath}{SDKBannerFile}";
    public static readonly string OfflineDBPath = $"{ResourcesPath}{OfflineDBFile}";
    public static readonly string PrefabsPath = $"{VRCrashPath}{PrefabsDirectoryName}";
    public static readonly string UnityPackagePath = $"{VRCrashPath}{UnityPackageDirectoryName}";
    public static readonly string BedroomPrefabVersionedPath = $"{PrefabsPath}{BedroomBaseExportingName}";
    public static readonly string BedroomUnityPackageVersionedPath = $"{UnityPackagePath}{BedroomBaseExportingName}";
    public static readonly string DiscordIconPath = $"{ResourcesPath}{DiscordImage}";
    public static readonly string TwitterIconPath = $"{ResourcesPath}{TwitterImage}";
    public static readonly string YoutubeImagePath = $"{ResourcesPath}{YoutubeImage}";
    public static readonly string PlayImagePath = $"{ResourcesPath}{PlayImage}";
    public static readonly string PauseImagePath = $"{ResourcesPath}{PauseImage}";

    public static readonly string PrefabExtension = ".prefab";
    public static readonly string UnityPackageExtension = ".unitypackage";
    public static readonly string PrefabExtensionPattern = "*.prefab";
    public static readonly string UnityPackageExtensionPattern = "*.unitypackage";
    public static readonly string VersionDelimiter = "_v";
    public static readonly string PrefabsDirectoryNameWihtoutAssets = "/VRCrash/Prefabs/";
    public static readonly string UnityPackageDirectoryNameWihtoutAssets = "/VRCrash/UnityPackages/";

}