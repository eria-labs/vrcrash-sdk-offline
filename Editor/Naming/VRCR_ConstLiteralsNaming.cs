﻿namespace VRCrash.Editor.Naming
{
    public class VRCR_ConstLiteralsNaming
    {
        public const string RobotoFont = "Roboto";
        public const string Dot = ".";
        public const string ThreeDots = "...";
        public const string Window = "window";
        public const string Empty = "";
        public const string Dollar = "$";
        public const char DollarChar = '$';
        public const string BackspaceAndDash = "\n - ";
        public const string UnityEngine = "UnityEngine.";
    }
}