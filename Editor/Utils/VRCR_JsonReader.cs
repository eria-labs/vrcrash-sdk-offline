﻿using UnityEngine;

public class VRCR_JSONReader
{
    public static T ReadJsonUsersFromPath<T>(TextAsset jsonFile)
    {
        return JsonUtility.FromJson<T>(jsonFile.text);
    }
}