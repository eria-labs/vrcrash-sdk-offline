﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class VRCR_EasterEgg
{
    public static void PlayClip(AudioClip clip)
    {

          Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
          Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
          MethodInfo method = audioUtilClass.GetMethod(
              "PlayClip",
              BindingFlags.Static | BindingFlags.Public,
              null,
              new[]
              {
                  typeof(AudioClip)
              },
              null
          );

          method.Invoke(
              null,
              new object[]
              {
                  clip
              }
          );
    }
    public static void StopClip(AudioClip clip)
    {

        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "StopClip",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new[]
            {
                  typeof(AudioClip)
            },
            null
        );

        method.Invoke(
            null,
            new object[]
            {
                  clip
            }
        );
    }

    public static bool isClipPlaying(AudioClip clip)
    {

        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "IsClipPlaying",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new[]
            {
                  typeof(AudioClip)
            },
            null
        );

       return (bool) method.Invoke(
            null,
            new object[]
            {
                  clip
            }
        );
    }
}