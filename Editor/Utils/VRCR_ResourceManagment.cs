﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class VRCR_ResourceManagment
{
    public VRCR_Users ReadJsonUsers()
    {
        return VRCR_JSONReader.ReadJsonUsersFromPath<VRCR_Users>(
            AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.OfflineDBPath, typeof(TextAsset)) as
                TextAsset);
    }

    public Texture2D LoadSDKBanner()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.SDKBannerPath,
            typeof(Texture2D)) as Texture2D;
    }

    public GameObject LoadBedroomPrefab(VRCR_User user)
    {
        return AssetDatabase.LoadAssetAtPath(GetBedroomTypePath(user), typeof(GameObject)) as GameObject;
    }

    public AudioClip LoadGnomeAudio()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.GnomeAudioPath, typeof(AudioClip)) as
            AudioClip;
    }
    public AudioClip LoadGenKeyMusic()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.GenKeyAudioPath, typeof(AudioClip)) as
            AudioClip;
    }

    public Texture2D LoadGnomeImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.GnomeImagePath,
            typeof(Texture2D)) as Texture2D;
    }

    public Texture2D LoadTwitterImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.TwitterIconPath,
            typeof(Texture2D)) as Texture2D;
    }
    public Texture2D LoadYoutubeImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.YoutubeImagePath,
            typeof(Texture2D)) as Texture2D;
    }
    public Texture2D LoadPlayImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.PlayImagePath,
            typeof(Texture2D)) as Texture2D;
    }
    public Texture2D LoadPauseImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.PauseImagePath,
            typeof(Texture2D)) as Texture2D;
    }
    public Texture2D LoadDiscordImage()
    {
        return AssetDatabase.LoadAssetAtPath(VRCR_ResourceManagmentNaming.DiscordIconPath,
            typeof(Texture2D)) as Texture2D;
    }

    public void GenerateVersionedBedroomUnityPackage(GameObject obj, int bedroomNumber, int version)
    {
        var prefabPath =
            $"{VRCR_ResourceManagmentNaming.BedroomPrefabVersionedPath}{bedroomNumber}{VRCR_ResourceManagmentNaming.VersionDelimiter}{version}{VRCR_ResourceManagmentNaming.PrefabExtension}";
        var unityPackagePath =
            $"{VRCR_ResourceManagmentNaming.BedroomUnityPackageVersionedPath}{bedroomNumber}{VRCR_ResourceManagmentNaming.VersionDelimiter}{version}{VRCR_ResourceManagmentNaming.UnityPackageExtension}";
        obj.name =
            $"{VRCR_ResourceManagmentNaming.BedroomBaseExportingName}{bedroomNumber}{VRCR_ResourceManagmentNaming.VersionDelimiter}{version}";
        PrefabUtility.SaveAsPrefabAsset(obj, prefabPath);
        AssetDatabase.ExportPackage(prefabPath, unityPackagePath,
            ExportPackageOptions.Recurse | ExportPackageOptions.IncludeDependencies);
    }

    private string GetBedroomTypePath(VRCR_User user)
    {
        if (user.Bedroom != null)
        {
            if (user.Bedroom.number < 6 || (user.Bedroom.number > 10 && user.Bedroom.number < 16))
                return VRCR_ResourceManagmentNaming.BedroomType1Path;

            return VRCR_ResourceManagmentNaming.BedroomType2Path;
        }

        return VRCR_ResourceManagmentNaming.BedroomType1Path; // Defaults to type1
    }

    public int SearchBedroomVersion()
    {
        var version = -1;
        FileInfo[] infoPrefabs;
        FileInfo[] infoUnityPackages;
        string[] folders = {"Assets/VRCrash/Prefabs", "Assets/VRCrash/UnityPackages"};
        if (!AreFoldersValid(folders)) return version;
        try
        {
            infoPrefabs =
                new DirectoryInfo(
                        $"{Application.dataPath}{VRCR_ResourceManagmentNaming.PrefabsDirectoryNameWihtoutAssets}")
                    .GetFiles(VRCR_ResourceManagmentNaming.PrefabExtensionPattern);

            infoUnityPackages =
                new DirectoryInfo(
                        $"{Application.dataPath}{VRCR_ResourceManagmentNaming.UnityPackageDirectoryNameWihtoutAssets}")
                    .GetFiles(VRCR_ResourceManagmentNaming.UnityPackageExtensionPattern);
            version = GetHigherVersion(infoPrefabs, GetHigherVersion(infoUnityPackages, version)) + 1;
        }
        catch (DirectoryNotFoundException e)
        {
            Debug.Log(e.Message); //to ignore warning
        }

        return version;
    }
    
    public bool AreFoldersValid(string[] folders)
    {
        foreach (var folder in folders)
        {
            if (!AssetDatabase.IsValidFolder(folder)) return false;
        }
        
        return true;
    }

    private int GetHigherVersion(FileInfo[] info, int version)
    {
        foreach (var f in info)
            if (f.Name.Contains(VRCR_ResourceManagmentNaming.BedroomBaseExportingName) &&
                int.TryParse(f.Name[f.Name.IndexOf('v') + 1].ToString(), out var versionTmp2))
                if (version < versionTmp2)
                    version = versionTmp2;

        return version;
    }
}