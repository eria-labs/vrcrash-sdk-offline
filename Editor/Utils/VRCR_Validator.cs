﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using VRCrash.Editor.Naming;
using Object = UnityEngine.Object;

public class VRCR_Validator
{
    private readonly List<Material> MatarialsUsed = new List<Material>();

    public bool AreComponentsValid(GameObject templateGameObject)
    {
        var invalidComponents = 
            templateGameObject.GetComponentsInChildren<Component>(true).Where(component => !IsValidComponent(component)).ToList();
        PrintInvalidComponentErrors(invalidComponents);
        return invalidComponents.Count == 0;
    }

    private void PrintInvalidComponentErrors(List<Component> components)
    {
        if (components.Count == 0) return;
        EditorGUILayout.HelpBox(VRCR_RestrictionsNaming.InvalidComponents + components.Count, MessageType.Error);
        foreach (var component in components)
        {
            EditorGUILayout.BeginHorizontal();
            var guiContent = new GUIContent(EditorGUIUtility.ObjectContent(component, component.GetType()));
            var guiStyle = GUI.skin.textArea;
            guiStyle.padding.left = 5;
            if(GUILayout.Button("",GUILayout.Width(20))) Selection.activeObject = component.gameObject;
            if (GUILayout.Button("X", GUILayout.Width(20))) Object.DestroyImmediate(component);
            GUILayout.Label(guiContent, guiStyle, GUILayout.ExpandWidth(true), GUILayout.Height(25));
            EditorGUILayout.EndHorizontal();
        }
    }
    
    public bool IsValidComponent(Component component)
    {
        string componentName = FormatComponentName(component);
        foreach (var validComponentName in VRCR_RestrictionsNaming.VALID_COMPONENTS)
            if (componentName.Equals(validComponentName)) return true;
        return false;
    }

    private string FormatComponentName(Component component)
    {
        var splitName = component.GetType().ToString().Split('.');
        return splitName[splitName.Length - 1];
    }

    public bool IsTrisCountValid(int trisCount)
    {
        if (trisCount > VRCR_RestrictionsNaming.MAX_TRIANGLES)
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.TrianglesNumber + trisCount +
                VRCR_RestrictionsNaming.SurpassedTrianglesMaximum + VRCR_RestrictionsNaming.MAX_TRIANGLES,
                MessageType.Error);
            return false;
        }

        if (trisCount >= VRCR_RestrictionsNaming.MAX_TRIANGLES)
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.TrianglesNumber + trisCount + VRCR_RestrictionsNaming.Maximum +
                VRCR_RestrictionsNaming.MAX_TRIANGLES,
                MessageType.Warning);
        }
        else
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.TrianglesNumber + trisCount + VRCR_RestrictionsNaming.Maximum +
                VRCR_RestrictionsNaming.MAX_TRIANGLES,
                MessageType.Info);
        }

        return true;
    }

    public bool IsMatCountValid(int matCount)
    {
        if (matCount > VRCR_RestrictionsNaming.MAX_MAT)
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.MaterialsNumber + matCount + VRCR_RestrictionsNaming.SurpassedMaterialsMaximum +
                VRCR_RestrictionsNaming.MAX_MAT,
                MessageType.Error);
            return false;
        }

        if (matCount >= VRCR_RestrictionsNaming.MAX_MAT / 2)
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.MaterialsNumber + matCount + VRCR_RestrictionsNaming.Maximum +
                VRCR_RestrictionsNaming.MAX_MAT, MessageType.Warning);
        }
        else
        {
            EditorGUILayout.HelpBox(
                VRCR_RestrictionsNaming.MaterialsNumber + matCount + VRCR_RestrictionsNaming.Maximum +
                VRCR_RestrictionsNaming.MAX_MAT, MessageType.Info);
        }

        return true;
    }

    public bool AreMatTypeValid(GameObject templateGameObject)
    {
        foreach (MeshRenderer mr in templateGameObject.GetComponentsInChildren(typeof(MeshRenderer)))
            if (!isValidShader(mr.sharedMaterial.shader.name))
                EditorGUILayout.HelpBox(VRCR_RestrictionsNaming.InvalidShader + mr.sharedMaterial.shader.name,
                      MessageType.Error);
        return true;
    }

    public bool isValidShader(String shaderName)
    {
        foreach (var validShaderName in VRCR_RestrictionsNaming.VALID_SHADERS)
            if (shaderName.Equals(validShaderName))
                return true;
  
        return false;
    }
    public int GetMatCount(GameObject templateGameObject)
    {
        MatarialsUsed.Clear();
        foreach (var component in templateGameObject.GetComponentsInChildren(typeof(MeshRenderer),true))
        {
            var mr = (MeshRenderer) component;
            if (!IgnoreGameObject(mr.gameObject.name))
                foreach (var mat in mr.sharedMaterials)
                    AddMaterialIfNew(mat);
        }

        return MatarialsUsed.Count;
    }

    public void AddMaterialIfNew(Material mat)
    {
        foreach (var usedMaterial in MatarialsUsed)
            if (usedMaterial.Equals(mat))
                return;

        MatarialsUsed.Add(mat);
    }

    public int GetTrisCount(GameObject templateGameObject)
    {
        var trianglesCount = 0;

        foreach (var component in templateGameObject.GetComponentsInChildren(typeof(MeshFilter),true))
        {
            var mf = (MeshFilter) component;
            trianglesCount += mf.sharedMesh.triangles.Length / 3;
        }

        return trianglesCount;
    }

    public bool IgnoreGameObject(string nameObj)
    {
        foreach (var nameL in new List<string>())
            if (nameL.Equals(nameObj))
                return true;
        return false;
    }

    public bool ValidateRestrictions(GameObject templateGameObject)
    {
        var trisCountValid = IsTrisCountValid(GetTrisCount(templateGameObject));
        var matCountValid = IsMatCountValid(GetMatCount(templateGameObject));
        var validMaterialType = AreMatTypeValid(templateGameObject);

        return AreComponentsValid(templateGameObject) && trisCountValid && matCountValid && validMaterialType;
    }

    public GameObject FindGameObjectByTag(string tag)
    {
        GameObject[] gameObjectsWithTag;
        try
        {
            gameObjectsWithTag = GameObject.FindGameObjectsWithTag(tag);
        }
        catch (Exception e)
        {

            e.Message.ToString();// to supress warning
            return null;
        }

        if (gameObjectsWithTag is null || gameObjectsWithTag.Length > 1 || gameObjectsWithTag.Length == 0) return null;
        return gameObjectsWithTag[0];
    }
}